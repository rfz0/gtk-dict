#include <stdlib.h>
#include <string.h>

#include <glib.h>

#include "dict.h"
#include "net.h"

#define DICT_BUFSIZ 1024
#ifndef DICT_DEFAULT_HOST
#define DICT_DEFAULT_HOST "dict.org"
#endif
#ifndef DICT_DEFAULT_PORT
#define DICT_DEFAULT_PORT 2628
#endif

typedef enum {
	DICT_STATE_FORMAT_CMD,
	DICT_STATE_SEND_CMD,
	DICT_STATE_READ_RESPONSE,
	DICT_STATE_HANDLE_RESPONSE,
	DICT_STATE_READ_RESULT,
	N_DICT_STATE
} state_t;

typedef gboolean (*state_func_t)(dict_t *, dict_cmd_t, gchar **, GError **);
typedef gchar *(*format_func_t)(dict_t *, const gchar *);
typedef gboolean (*response_func_t)(gint);

static gchar *format_define(dict_t *, const gchar *);
static gchar *format_match(dict_t *, const gchar *);
static gchar *format_quit(dict_t *, const gchar *);
static gchar *format_show_databases(dict_t *, const gchar *);
static gchar *format_show_info(dict_t *, const gchar *);
static gchar *format_show_server(dict_t *, const gchar *);
static gchar *format_show_strat(dict_t *, const gchar *);
static gboolean state_format(dict_t *, dict_cmd_t, gchar **, GError **);
static gboolean state_send(dict_t *, dict_cmd_t, gchar **, GError **);
static gboolean state_read_response(dict_t *, dict_cmd_t, gchar **, GError **);
static gboolean state_handle_response(dict_t *, dict_cmd_t, gchar **, GError **);
static gboolean state_read_result(dict_t *, dict_cmd_t, gchar **, GError **);
static gboolean handle_response_define(gint);
static gboolean handle_response_match(gint);
static gboolean handle_response_show_databases(gint);
static gboolean handle_response_show_info(gint);
static gboolean handle_response_show_server(gint);
static gboolean handle_response_show_strat(gint);
static gboolean handle_response_quit(gint);
static gboolean read_result_slist(dict_t *, GError **);

static GSList **response = NULL;

static state_func_t
state_handler[N_DICT_STATE] =  {
	[DICT_STATE_FORMAT_CMD] = state_format,
	[DICT_STATE_SEND_CMD] = state_send,
	[DICT_STATE_READ_RESPONSE] = state_read_response,
	[DICT_STATE_HANDLE_RESPONSE] = state_handle_response,
	[DICT_STATE_READ_RESULT] = state_read_result,
};

static format_func_t
format_handler[N_DICT_CMD] = {
	[DICT_CMD_AUTH] = NULL,
	[DICT_CMD_CLIENT] = NULL,
	[DICT_CMD_DEFINE] = format_define,
	[DICT_CMD_HELP] = NULL,
	[DICT_CMD_MATCH] = format_match,
	[DICT_CMD_OPTION] = NULL,
	[DICT_CMD_QUIT] = format_quit,
	[DICT_CMD_SASLAUTH] = NULL,
	[DICT_CMD_SHOW_DATABASES] = format_show_databases,
	[DICT_CMD_SHOW_INFO] = format_show_info,
	[DICT_CMD_SHOW_SERVER] = format_show_server,
	[DICT_CMD_SHOW_STRAT] = format_show_strat,
	[DICT_CMD_STATUS] = NULL
};

static response_func_t
response_handler[N_DICT_CMD] = {
	[DICT_CMD_AUTH] = NULL,
	[DICT_CMD_CLIENT] = NULL,
	[DICT_CMD_DEFINE] = handle_response_define,
	[DICT_CMD_HELP] = NULL,
	[DICT_CMD_MATCH] = handle_response_match,
	[DICT_CMD_OPTION] = NULL,
	[DICT_CMD_QUIT] = handle_response_quit,
	[DICT_CMD_SASLAUTH] = NULL,
	[DICT_CMD_SHOW_DATABASES] = handle_response_show_databases,
	[DICT_CMD_SHOW_INFO] = handle_response_show_info,
	[DICT_CMD_SHOW_SERVER] = handle_response_show_server,
	[DICT_CMD_SHOW_STRAT] = handle_response_show_strat,
	[DICT_CMD_STATUS] = NULL
};

gboolean
dict_open(dict_t *d,
		GError **error)
{
	gboolean status = TRUE;
	if (net_open(d->net, error) == FALSE)
		return FALSE;
	gchar *buf;
	if (net_read_line(d->net, &buf, &(gsize){0}, error) == FALSE)
		goto error;
	if (dict_cmd_response_code(buf) !=
			DICT_RESPONSE_220_TEXT_CAPABILITIES_MSGID) {
		g_warning("%s", buf);
error:
		(void)dict_close(d, NULL);
		status = FALSE;
	}
	g_free(buf);

	return status;
}

gboolean
dict_close(dict_t *d,
		GError **error)
{
	dict_req_t *req = dict_req_new(DICT_CMD_QUIT, NULL);
	(void)dict_cmd(d, req, NULL);
	dict_req_destroy(req);

	return net_close(d->net, NULL);
}

gboolean
dict_cmd(dict_t *d,
		dict_req_t *req,
		GError **error)
{
	response = &req->res;
	state_t state = DICT_STATE_FORMAT_CMD;
	gchar *buf = g_strdup_printf("\"%s\"", req->arg);
	gboolean status;
	do {
		status = FALSE;
		gpointer data = g_async_queue_try_pop(d->cancel_queue);
		if (GPOINTER_TO_INT(data) == DICT_CMD_CANCEL)
			break;
		if (state_handler[state])
			status = (*state_handler[state++])(d, req->cmd, &buf,
					error);
	} while (status);
	if (buf)
		g_free(buf);

	return status;
}

void
dict_cmd_cancel(dict_t *d)
{
	g_async_queue_push(d->cancel_queue,
			GINT_TO_POINTER(DICT_CMD_CANCEL));
	g_cancellable_cancel(d->net->cancellable);
}

gint
dict_cmd_response_code(const gchar *string)
{
	gchar *buf = g_strndup(string, 3);
	gint code = atoi(buf);
	g_free(buf);
	return code;
}

void
dict_cancel_clear(dict_t *d)
{
	g_cancellable_reset(d->net->cancellable);
	while (g_async_queue_try_pop(d->cancel_queue))
		;
}

void
dict_destroy(dict_t *d)
{
	g_async_queue_unref(d->cancel_queue);
	net_destroy(d->net);
	if (d->strat)
		g_free(d->strat);
	if (d->db)
		g_free(d->db);
	g_free(d);
}

const gchar *
dict_get_host(dict_t *d)
{
	return net_get_host(d->net);
}

dict_t *
dict_new(void)
{
	dict_t *d = g_new0(dict_t, 1);
	d->db = g_strdup(DICT_CMD_ARG_DATABASE_ALL_ALL);
	d->strat = g_strdup(DICT_CMD_ARG_STRAT_SERVER);
	d->net = net_new(DICT_DEFAULT_HOST, DICT_DEFAULT_PORT, 1,
			G_DATA_STREAM_NEWLINE_TYPE_CR_LF);
	d->cancel_queue = g_async_queue_new();
	return d;
}

void
dict_req_destroy(dict_req_t *req)
{
	if (req == NULL)
		return;
	if (req->arg)
		g_free(req->arg);
	g_free(req);
}

dict_req_t *
dict_req_new(dict_cmd_t cmd, gchar *arg)
{
	dict_req_t *req = g_new0(dict_req_t, 1);
	req->cmd = cmd;
	req->arg = arg;
	return req;
}

void
dict_set_db(dict_t *d,
		const gchar *db)
{
	if (d->db)
		g_free(d->db);
	if (g_ascii_strcasecmp(db, "default") == 0)
		d->db = g_strdup(DICT_CMD_ARG_DATABASE_ALL_ALL);
	else
		d->db = g_strdup(db);
}

void
dict_set_host(dict_t *d, const gchar *host)
{
	net_set_host(d->net, host);
}

void
dict_set_strat(dict_t *d,
		const gchar *strat)
{
	if (d->strat)
		g_free(d->strat);
	if (g_ascii_strcasecmp(strat, "default") == 0)
		d->strat = g_strdup(DICT_CMD_ARG_STRAT_SERVER);
	else
		d->strat = g_strdup(strat);
}

gboolean
state_format(dict_t *d,
		dict_cmd_t cmd,
		gchar **buf,
		GError **error)
{
	*buf = (*format_handler[cmd])(d, *buf);
	return (*buf == NULL) ? FALSE : TRUE;
}

gboolean
state_send(dict_t *d,
		dict_cmd_t cmd,
		gchar **buf,
		GError **error)
{
	return net_write(d->net, *buf, error);
}

gboolean
state_read_response(dict_t *d,
		dict_cmd_t cmd,
		gchar **buf,
		GError **error)
{
	if (*buf)
		g_free(*buf);
	*buf = NULL;
	return net_read_line(d->net, buf, &(gsize){0}, error);
}

gboolean
state_handle_response(dict_t *d,
		dict_cmd_t cmd,
		gchar **buf,
		GError **error)
{
	gint response_code = dict_cmd_response_code(*buf);
	switch (response_code) {
		case DICT_RESPONSE_420_SERVER_TEMPORARILY_UNAVAILABLE:
		case DICT_RESPONSE_421_SERVER_SHUTTING_DOWN:
		case DICT_RESPONSE_500_SYNTAX_ERROR_COMMAND_NOT_RECOGNIZED:
		case DICT_RESPONSE_501_SYNTAX_ERROR_ILLEGAL_PARAMETER:
		case DICT_RESPONSE_502_COMMAND_NOT_IMPLEMENTED:
		case DICT_RESPONSE_503_COMMAND_PARAMETER_NOT_IMPLEMENTED:
			return FALSE;
	}
	if (response_handler[cmd])
		if ((*response_handler[cmd])(response_code) == FALSE)
			return FALSE;

	return TRUE;
}

gboolean
state_read_result(dict_t *d,
		dict_cmd_t cmd,
		gchar **buf,
		GError **error)
{
	if (*buf)
		g_free(*buf);
	*buf = NULL;
	return read_result_slist(d, error);
}

gchar *
format_define(dict_t *d,
		const gchar *word)
{
	return g_strdup_printf("DEFINE %s %s",
			d->db,
			word);
}

gchar *
format_match(dict_t *d,
		const gchar *word)
{
	return g_strdup_printf("MATCH %s %s %s",
			d->db,
			d->strat,
			word);
}

gchar *
format_show_databases(dict_t *d,
		const gchar *word)
{
	return g_strdup("SHOW DATABASES");
}

gchar *
format_show_info(dict_t *d,
		const gchar *word)
{
	return g_strdup("SHOW INFO");
}

gchar *
format_show_server(dict_t *d,
		const gchar *word)
{
	return g_strdup("SHOW SERVER");
}

gchar *
format_show_strat(dict_t *d,
		const gchar *word)
{
	return g_strdup("SHOW STRAT");
}

gchar *
format_quit(dict_t *d,
		const gchar *word)
{
	return g_strdup("QUIT");
}

gboolean
handle_response_define(gint response_code)
{
	switch (response_code) {
		case DICT_RESPONSE_150_DEFINITIONS_RETRIEVED:
			return TRUE;
		case DICT_RESPONSE_550_INVALID_DATABASE:
		case DICT_RESPONSE_552_NO_MATCH:
		default:
			return FALSE;
	}
}

gboolean
handle_response_match(gint response_code)
{
	switch (response_code) {
		case DICT_RESPONSE_152_MATCHES_FOUND:
			return TRUE;
		case DICT_RESPONSE_550_INVALID_DATABASE:
		case DICT_RESPONSE_551_INVALID_STRAT:
		case DICT_RESPONSE_552_NO_MATCH:
		default:
			return FALSE;
	}
}

gboolean
handle_response_show_databases(gint response_code)
{
	switch (response_code) {
		case DICT_RESPONSE_110_DATABASES_PRESENT:
			return TRUE;
		case DICT_RESPONSE_554_NO_DATABASES_PRESENT:
		default:
			return FALSE;
	}
}

gboolean
handle_response_show_info(gint response_code)
{
	switch (response_code) {
		case DICT_RESPONSE_112_DATABASE_INFORMATION:
			return TRUE;
		case DICT_RESPONSE_550_INVALID_DATABASE:
		default:
			return FALSE;
	}
}

gboolean
handle_response_show_server(gint response_code)
{
	switch (response_code) {
		case DICT_RESPONSE_114_SERVER_INFORMATION:
			return TRUE;
		default:
			return FALSE;
	}
}

gboolean
handle_response_show_strat(gint response_code)
{
	switch (response_code) {
		case DICT_RESPONSE_111_STRATEGIES_AVAILABLE:
			return TRUE;
		case DICT_RESPONSE_555_NO_STRATEGIES_AVAILABLE:
		default:
			return FALSE;
	}
}

gboolean
handle_response_quit(gint response_code)
{
	return TRUE;
}

gboolean
read_result_slist(dict_t *d,
		GError **error)
{
	GSList *slist = NULL;
	gchar *buf;
	while (net_read_line(d->net, &buf, &(gsize){0}, error) == TRUE) {
		if (dict_cmd_response_code(buf) == DICT_RESPONSE_250_OK) {
			g_free(buf);
			break;
		}
		if (g_ascii_strncasecmp(buf, DICT_EOT, strlen(DICT_EOT) + 1) != 0)
			slist = g_slist_prepend(slist, g_strdup(buf));
		g_free(buf);
	}
	slist = g_slist_reverse(slist);
	*response = slist;
	if (error && *error)
		return FALSE;
	return TRUE;
}

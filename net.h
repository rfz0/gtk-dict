#ifndef NET_H
#define NET_H

#include <glib.h>
#include <gio/gio.h>

typedef struct {
	GCancellable *cancellable;
	GSocketConnection *connection;
	gchar *host;
	guint timeout;
	guint16 port;
	struct {
		GDataInputStream *data_input;
		GDataOutputStream *data_output;
		GDataStreamNewlineType newline_type;
		GInputStream *input;
		GOutputStream *output;
		const gchar *newline;
	} ios;
} net_t;

gboolean net_close(net_t *, GError **);
void net_destroy(net_t *);
net_t *net_new(const gchar *, guint16 , guint, GDataStreamNewlineType);
gboolean net_open(net_t *socket, GError **);
gboolean net_read_line(net_t *socket, char **, gsize *, GError **);
const gchar *net_get_host(net_t *);
void net_set_host(net_t *, const gchar *);
gboolean net_write(net_t *, const gchar *, GError **);

#endif

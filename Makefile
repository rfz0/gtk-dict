TARGET := define
VERSION := 0.1
CC := cc
LIBS := gtk+-2.0 gthread-2.0
CFLAGS += -g -std=c99 -Wall -Wextra -Wno-unused-parameter `pkg-config --cflags $(LIBS)`
CFLAGS += -DTARGET=\"$(TARGET)\" -DVERSION=\"$(VERSION)\"
LDFLAGS := `pkg-config --libs $(LIBS)`
ICON := icon.h
SRCS := $(wildcard *.c)
OBJS := $(patsubst %.c,%.o,$(SRCS))

all: $(TARGET)

$(TARGET): $(ICON) $(OBJS) 
	$(CC) $(OBJS) -o $@ $(LDFLAGS)

$(ICON): icon.png
	gdk-pixbuf-csource --raw --name=icon $< > $@

%.o : %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(TARGET) $(OBJS) $(ICON)

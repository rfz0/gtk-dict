#include <glib.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <ctype.h>
#include <string.h>

#include "compat.h"
#include "gui.h"
#include "license.h"

static void entry_insert_text(GtkEntry *, const gchar *, gint, gint *,
		gpointer);
static void menu_popup(GtkWidget *, GdkEventButton *, const gchar *, const
		gchar *, const GtkActionEntry *, guint, const gchar *);
static void text_buffer_clear_all(GtkTextBuffer *);
static void text_buffer_highlight_all(GtkTextBuffer *, const gchar *);
static void text_buffer_highlight_clear(GtkTextBuffer *);
static gboolean text_buffer_is_link_start(gunichar, gpointer);
static gboolean text_buffer_is_link_end(gunichar, gpointer);
static void text_buffer_linkify(GtkTextBuffer *, GtkTextIter, GtkTextIter);
static void text_buffer_search(GtkTextBuffer *, GtkTextIter *, const gchar *,
		gboolean);
static gboolean text_view_event_after(GtkWidget *, GdkEvent *);
static gboolean text_view_event_key_press(GtkWidget *, GdkEventKey *);
static gboolean text_view_event_motion_notify(GtkWidget *, GdkEventMotion *);
static gboolean text_view_event_visibility_notify(GtkWidget *,
		GdkEventVisibility *);
static void text_view_follow_if_link(GtkWidget *, GtkTextIter *);
static void text_view_set_cursor(GtkTextView *, gint, gint);
static gboolean tree_view_button_press(GtkWidget *, GdkEventButton *,
		gpointer);
static void tree_view_collapse_all(GtkWidget *, gpointer);
static void tree_view_expand_all(GtkWidget *, gpointer);
static void tree_view_insert_list(GtkTreeView *, const gchar *, GSList *);
static void tree_view_menu_popup(GtkWidget *, GdkEventButton *, gpointer);
static gboolean tree_view_selection_func(GtkTreeSelection *,
		GtkTreeModel *, GtkTreePath *, gboolean, gpointer);

static GdkCursor *hand_cursor, *regular_cursor;

static GtkActionEntry menu_popup_entries[] = {
	{ "ExpandAll", NULL, "_Expand All", NULL, "Expand All",
		G_CALLBACK(tree_view_expand_all) },
	{ "CollapseAll", NULL, "_Collapse All", NULL, "Collapse All",
		G_CALLBACK(tree_view_collapse_all) },
};

static const gchar *const menu_popup_entries_info =
"<ui>"
" <popup action='TreeViewMenu'>"
" <menuitem action='ExpandAll'/>"
" <separator/>"
" <menuitem action='CollapseAll'/>"
" </popup>"
"</ui>";

void
combo_box_insert(gpointer data,
		gpointer user_data)
{
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(user_data),
			(gchar *)data);
}

void
dialog_about(void)
{
	GtkWidget *dialog = gtk_about_dialog_new();
	gtk_about_dialog_set_name(GTK_ABOUT_DIALOG(dialog), TARGET);
	gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), VERSION);
	gtk_about_dialog_set_license(GTK_ABOUT_DIALOG(dialog), LICENSE);
	gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), COMMENTS);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

void
dialog_info(GtkWidget *parent,
		const gchar *title,
		const gchar *text)
{
	GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(parent),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_INFO,
			GTK_BUTTONS_OK,
			"%s", text);
	(void)gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

gchar *
dialog_input(const gchar *title, const gchar *entry_text)
{
	GtkWidget *dialog = gtk_dialog_new_with_buttons(title, NULL,
			GTK_DIALOG_MODAL,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
			NULL);
	GtkWidget *content_area =
		gtk_dialog_get_content_area(GTK_DIALOG(dialog));
	GtkWidget *entry = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(entry), entry_text);
	gtk_entry_set_overwrite_mode(GTK_ENTRY(entry), TRUE);
	gtk_container_add(GTK_CONTAINER(content_area), entry);
	gtk_widget_show(entry);
	gint result = gtk_dialog_run(GTK_DIALOG(dialog));
	gchar *text = NULL;
	if ((result == GTK_RESPONSE_ACCEPT) &&
		(gtk_entry_get_text_length(GTK_ENTRY(entry)) > 0))
			text = g_strdup(gtk_entry_get_text(GTK_ENTRY(entry)));
	gtk_widget_destroy(dialog);
	return text;
}

void
dialog_save(GtkWindow *parent,
		gchar **filename)
{
	static const gchar *current_folder = NULL;
	GtkWidget *dialog = gtk_file_chooser_dialog_new("Save File", parent,
			GTK_FILE_CHOOSER_ACTION_SAVE,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
			NULL);
	if (current_folder == NULL)
		current_folder = g_get_home_dir();
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog),
			current_folder);
	gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog),
			TRUE);
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
		*filename =
			gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		current_folder =
			gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER(dialog));
	}
	gtk_widget_destroy(dialog);
}

void
entry_changed(GtkEditable *editable,
		GtkWidget *button)
{
	guint length;
	g_object_get(editable, "text-length", &length, NULL);
	gboolean sensitive = gtk_widget_get_sensitive(button);
	if (length == 0 && sensitive)
		gtk_widget_set_sensitive(button, FALSE);
	else
		gtk_widget_set_sensitive(button, TRUE);
}

void
entry_clear(GtkWidget *widget,
		gpointer data)
{
	GtkWidget *entry = GTK_WIDGET(data);
	GtkEntryBuffer *entry_buffer = gtk_entry_get_buffer(GTK_ENTRY(entry));
	gtk_entry_buffer_delete_text(entry_buffer, 0, -1);
	gtk_widget_grab_focus(entry);
}

void
entry_completion_append(GtkEntry *entry,
		const gchar *word)
{
	if (word == NULL)
		return;

	GtkEntryCompletion *entry_completion = gtk_entry_get_completion(entry);
	GtkTreeModel *tree_model = gtk_entry_completion_get_model(entry_completion);
	(void)tree_model_append(tree_model, NULL, 0, word, TRUE);
}

void
entry_completion_clear(GtkWidget *entry)
{
	GtkEntryCompletion *entry_completion = gtk_entry_get_completion(GTK_ENTRY(entry));
	GtkTreeModel *tree_model = gtk_entry_completion_get_model(entry_completion);
	tree_model_clear(tree_model);
}

void
entry_insert_text(GtkEntry *entry,
		const gchar *text,
		gint text_length,
		gint *position,
		gpointer data)
{
	GtkEditable *editable = GTK_EDITABLE(entry);
	gchar *new_text = g_new(gchar, text_length);
	int new_text_length = 0;
	for (int i = 0; i < text_length; i++) {
		if (strchr((gchar *)data, text[i]) || iscntrl(text[i]))
			continue;
		new_text[new_text_length++] = text[i];
	}
	if (new_text_length > 0) {
		g_signal_handlers_block_by_func(G_OBJECT(editable),
				G_CALLBACK(entry_insert_text), data);
		gtk_editable_insert_text(editable, new_text, new_text_length,
				position);
		g_signal_handlers_unblock_by_func(G_OBJECT(editable),
				G_CALLBACK(entry_insert_text), data);
	}
	g_signal_stop_emission_by_name(G_OBJECT(editable), "insert_text");
	g_free(new_text);
}

GtkWidget *
entry_new(gchar *restrict_charset)
{
	GtkWidget *entry = gtk_entry_new();
	if (restrict_charset)
		g_signal_connect(GTK_OBJECT(entry), "insert_text",
				G_CALLBACK(entry_insert_text),
				restrict_charset);

	GtkEntryCompletion *entry_completion = gtk_entry_completion_new();
	gtk_entry_set_completion(GTK_ENTRY(entry), entry_completion);
	g_object_unref(entry_completion);

	GtkTreeModel *tree_model = GTK_TREE_MODEL(gtk_list_store_new(1,
				G_TYPE_STRING));
	gtk_entry_completion_set_model(entry_completion, tree_model);
	g_object_unref(tree_model);

	gtk_entry_completion_set_text_column(entry_completion, 0);

	return entry;
}

GtkWidget *
menu_new(GtkWidget *parent,
		const gchar *name,
		const gchar *path,
		const GtkActionEntry *entries,
		guint n_entries,
		const gchar *entries_info)
{
	GtkUIManager *ui_manager = g_object_get_data(G_OBJECT(parent), "ui-manager");
	if (ui_manager == NULL)
		ui_manager = ui_manager_new(parent, name, entries, n_entries,
				entries_info);
	GtkWidget *menu = gtk_ui_manager_get_widget(ui_manager, path);

	return menu;
}

void
menu_popup(GtkWidget *parent,
		GdkEventButton *event_button,
		const gchar *name,
		const gchar *path,
		const GtkActionEntry *entries,
		guint n_entries,
		const gchar *entries_info)
{
	GtkWidget *menu = menu_new(parent, name, path, entries, n_entries, entries_info);
	gtk_widget_show_all(menu);
	guint button;
	guint32 activate_time;
	if (event_button) {
		button = event_button->button;
		activate_time = event_button->time;
	} else {
		button = 0;
		activate_time = gdk_event_get_time((GdkEvent *)event_button);
	}
	gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, button,
			activate_time);
}

GdkPixbuf *
pixbuf_from_inline(const guint8 *data)
{
	GdkPixbuf *pixbuf;
	GError *error = NULL;
	if ((pixbuf = gdk_pixbuf_new_from_inline(-1, data, FALSE, &error)) ==
			NULL)
		if (error) {
			g_warning("%s", error->message);
			g_error_free(error);
		}

	return pixbuf;
}

void
statusbar_write(GtkStatusbar *statusbar,
		const gchar *fmt,
		...)
{
	va_list ap;
	va_start(ap, fmt);
	gchar *text = g_strdup_vprintf(fmt, ap);
	va_end(ap);
	gdk_threads_enter();
	gtk_statusbar_pop(statusbar, 0);
	gtk_statusbar_push(statusbar, 1, text);
	gdk_flush();
	gdk_threads_leave();
	g_free(text);

}

void
text_buffer_clear_all(GtkTextBuffer *text_buffer)
{
	GtkTextIter start, end;
	gtk_text_buffer_get_bounds(text_buffer, &start, &end);
	gtk_text_buffer_delete(text_buffer, &start, &end);
}

void
text_buffer_highlight_all(GtkTextBuffer *buffer,
		const gchar *str)
{
	GtkTextTagTable *tag_table = gtk_text_buffer_get_tag_table(buffer);
	GtkTextTag *tag = gtk_text_tag_table_lookup(tag_table, "highlight");
	if (tag == NULL)
		tag = gtk_text_buffer_create_tag(buffer, "highlight",
				"background", "yellow", NULL);
	GtkTextIter iter;
	gtk_text_buffer_get_start_iter(buffer, &iter);
	GtkTextIter match_start, match_end;
	while (gtk_text_iter_forward_search(&iter, str, 0, &match_start,
				&match_end, NULL)) {
		gtk_text_buffer_apply_tag(buffer, tag, &match_start,
				&match_end);
		iter = match_end;
	}
}

void
text_buffer_highlight_clear(GtkTextBuffer *buffer)
{
	GtkTextTagTable *text_tag_table = gtk_text_buffer_get_tag_table(buffer);
	GtkTextTag *tag = gtk_text_tag_table_lookup(text_tag_table, "highlight");
	if (tag) {
		GtkTextIter start, end;
		gtk_text_buffer_get_bounds(buffer, &start, &end);
		gtk_text_buffer_remove_tag(buffer, tag, &start, &end);
	}
	GtkTextMark *insert = gtk_text_buffer_get_mark(buffer, "insert");
	GtkTextIter iter;
	gtk_text_buffer_get_iter_at_mark(buffer, &iter, insert);
	gtk_text_buffer_move_mark_by_name(buffer, "selection_bound", &iter);
}

void
text_buffer_insert_header(GtkTextBuffer *buffer,
		GtkTextIter *iter,
		const gchar *header)
{
	GtkTextTagTable *tag_table = gtk_text_buffer_get_tag_table(buffer);
	GtkTextTag *tag = gtk_text_tag_table_lookup(tag_table, "bold");
	if (tag == NULL)
		tag = gtk_text_buffer_create_tag(buffer, "bold", "weight",
				PANGO_WEIGHT_BOLD, NULL);
	gtk_text_buffer_insert_with_tags(buffer, iter, header, -1, tag, NULL);
	gtk_text_buffer_insert(buffer, iter, "\n", 1);
}

void
text_buffer_insert_slist(GtkTextBuffer *buffer,
		GtkTextIter *iter,
		GSList *slist)
{
	for (GSList *elem = slist; elem; elem = elem->next) {
		gtk_text_buffer_insert(buffer, iter, (gchar *)elem->data, -1);
		gtk_text_buffer_insert(buffer, iter, "\n", 1);
	}
}

gboolean
text_buffer_is_link_start(gunichar ch, gpointer data)
{
	return ch == G_TOKEN_LEFT_CURLY;
}

gboolean
text_buffer_is_link_end(gunichar ch, gpointer data)
{
	return ch == G_TOKEN_RIGHT_CURLY;
}

void
text_buffer_linkify(GtkTextBuffer *buffer,
		GtkTextIter start,
		GtkTextIter end)
{
	GtkTextTagTable *tag_table = gtk_text_buffer_get_tag_table(buffer);
	GtkTextTag *tag_hidden = gtk_text_tag_table_lookup(tag_table, "hidden");
	if (tag_hidden == NULL)
		tag_hidden = gtk_text_buffer_create_tag(buffer, "hidden",
				"invisible", TRUE, NULL);
	GtkTextIter iter_1 = start;
	while (gtk_text_iter_forward_find_char(&iter_1,
				text_buffer_is_link_start, NULL, NULL))
	{
		GtkTextIter iter_2 = iter_1;
		while
			(text_buffer_is_link_start(gtk_text_iter_get_char(&iter_2),
						   NULL))
			if (gtk_text_iter_forward_char(&iter_2) == FALSE)
				break;
		gtk_text_buffer_apply_tag(buffer, tag_hidden, &iter_1, &iter_2);

		if (gtk_text_iter_forward_find_char(&iter_1,
					text_buffer_is_link_end, NULL, NULL) ==
				FALSE)
			break;
		GtkTextTag *tag_link = gtk_text_buffer_create_tag(buffer,
				NULL,
				"foreground", "blue",
				"underline", PANGO_UNDERLINE_SINGLE,
				NULL);
		gtk_text_buffer_apply_tag(buffer, tag_link, &iter_2, &iter_1);
		gchar *text = gtk_text_buffer_get_text(buffer, &iter_2, &iter_1,
				FALSE);
		g_object_set_data_full(G_OBJECT(tag_link), "data",
				g_strdup(text), (GDestroyNotify)g_free);
		g_free(text);
		iter_2 = iter_1;
		do
			if (gtk_text_iter_forward_char(&iter_2) == FALSE)
				break;
		while (text_buffer_is_link_end(gtk_text_iter_get_char(&iter_2),
					NULL));
		gtk_text_buffer_apply_tag(buffer, tag_hidden, &iter_1, &iter_2);
	}
}

gboolean
text_buffer_save(GtkTextBuffer *buffer,
		GtkWidget *parent)
{
	if (gtk_text_buffer_get_char_count(buffer) == 0) {
		dialog_info(parent, "File Save",
				"Text buffer is empty.");
		return FALSE;
	}
	gchar *filename = NULL;
	dialog_save(GTK_WINDOW(parent), &filename);
	if (filename == NULL)
		return FALSE;
	GtkTextIter start, end;
	gtk_text_buffer_get_bounds(buffer, &start, &end);
	gchar *contents = gtk_text_buffer_get_text(buffer, &start, &end, TRUE);
	GError *error = NULL;
	if (g_file_set_contents(filename, contents, -1, &error) == FALSE) {
		if (error) {
			g_warning("%s", error->message);
			GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(parent),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_ERROR,
					GTK_BUTTONS_CLOSE,
					"%s: %s",
					filename,
					error->message);
			gtk_dialog_run(GTK_DIALOG(dialog));
			gtk_widget_destroy(dialog);
			g_error_free(error);
		}
	}
	g_free(contents);
	g_free(filename);

	return TRUE;
}

void
text_buffer_search(GtkTextBuffer *buffer,
		GtkTextIter *iter,
		const gchar *text,
		gboolean forward)
{
	GtkTextIter start, end;
	gboolean found;
	if (forward)
		found = gtk_text_iter_forward_search(iter, text, 0, &start,
				&end, NULL);
	else
		found = gtk_text_iter_backward_search(iter, text, 0, &start,
				&end, NULL);
	if (found) {
		gtk_text_buffer_select_range(buffer, &start, &end);
		gtk_text_buffer_create_mark(buffer, "search", &end, FALSE);
	}
}

void
text_view_clear(GtkTextView *text_view)
{
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(text_view);
	text_buffer_clear_all(buffer);
	if (gtk_text_buffer_get_mark(buffer, "search")) {
		GtkTextIter iter;
		gtk_text_buffer_get_start_iter(buffer, &iter);
		gtk_text_buffer_move_mark_by_name(buffer, "search", &iter);
	}
}

GtkWidget *
text_view_new(void (*follow_link)(gpointer))
{
	GtkWidget *text_view = gtk_text_view_new();
	gtk_text_view_set_editable(GTK_TEXT_VIEW(text_view), FALSE);
	hand_cursor = gdk_cursor_new(GDK_HAND2);
	regular_cursor = gdk_cursor_new(GDK_XTERM);
	g_object_set_data(G_OBJECT(text_view), "follow_link", follow_link);
	g_object_connect(G_OBJECT(text_view),
			"signal::event-after",
			G_CALLBACK(text_view_event_after), NULL,
			"signal::key-press-event",
			G_CALLBACK(text_view_event_key_press), NULL,
			"signal::motion-notify-event",
			G_CALLBACK(text_view_event_motion_notify), NULL,
			"signal::visibility-notify-event",
			G_CALLBACK(text_view_event_visibility_notify),
			NULL, NULL);

	return text_view;
}

gboolean
text_view_event_after(GtkWidget *text_view,
		GdkEvent *event)
{
	if (event->type != GDK_BUTTON_RELEASE)
		return FALSE;
	GdkEventButton *event_button = (GdkEventButton *)event;
	if (event_button->button != 1)
		return FALSE;

	GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
	GtkTextIter start, end;
	gtk_text_buffer_get_selection_bounds(buffer, &start, &end);
	if (gtk_text_iter_get_offset(&start) != gtk_text_iter_get_offset(&end))
		return FALSE;
	gint x, y;
	gtk_text_view_window_to_buffer_coords(GTK_TEXT_VIEW(text_view),
			GTK_TEXT_WINDOW_WIDGET,
			event_button->x, event_button->y,
			&x, &y);
	GtkTextIter iter;
	gtk_text_view_get_iter_at_location(GTK_TEXT_VIEW(text_view), &iter, x,
			y);
	text_view_follow_if_link(text_view, &iter);

	return FALSE;
}

gboolean
text_view_event_key_press(GtkWidget *text_view,
		GdkEventKey *event)
{
	if ((event->keyval == GDK_Return) ||
		(event->keyval == GDK_KP_Enter)) {
		GtkTextBuffer *buffer =
			gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
		GtkTextIter iter;
		gtk_text_buffer_get_iter_at_mark(buffer, &iter,
				gtk_text_buffer_get_insert(buffer));
		text_view_follow_if_link(text_view, &iter);
	}

	return FALSE;
}

gboolean
text_view_event_motion_notify(GtkWidget *text_view,
		GdkEventMotion *event)
{
	gint x, y;
	gtk_text_view_window_to_buffer_coords(GTK_TEXT_VIEW(text_view),
			GTK_TEXT_WINDOW_WIDGET, event->x, event->y, &x, &y);
	text_view_set_cursor(GTK_TEXT_VIEW(text_view), x, y);
	gdk_window_get_pointer(text_view->window, NULL, NULL, NULL);

	return FALSE;
}

gboolean
text_view_event_visibility_notify(GtkWidget *text_view,
		GdkEventVisibility *event)
{
	text_view_update_cursor(text_view);

	return FALSE;
}

void
text_view_follow_if_link(GtkWidget *text_view,
		GtkTextIter *iter)
{
	GSList *slist = gtk_text_iter_get_tags(iter);
	for (GSList *elem = slist; elem; elem = elem->next) {
		GtkTextTag *tag = elem->data;
		gchar *data = g_object_get_data(G_OBJECT(tag), "data");
		if (data) {
			void (*follow_link)(gpointer) =
				g_object_get_data(G_OBJECT(text_view), "follow_link");
			follow_link(data);
		}
	}
	if (slist)
		g_slist_free(slist);
}

void
text_view_highlight_all(GtkTextView *text_view)
{
	GtkWidget *entry = g_object_get_data(G_OBJECT(text_view), "entry");
	if (gtk_entry_get_text_length(GTK_ENTRY(entry)) == 0)
		return;
	const gchar *text = gtk_entry_get_text(GTK_ENTRY(entry));
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(text_view);
	text_buffer_highlight_all(buffer, text);
}

void
text_view_highlight_clear(GtkTextView *text_view)
{
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(text_view);
	text_buffer_highlight_clear(buffer);
}

void
text_view_highlight_toggle(GtkWidget *widget,
		gpointer data)
{
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)))
		text_view_highlight_all(GTK_TEXT_VIEW(data));
	else
		text_view_highlight_clear(GTK_TEXT_VIEW(data));
}

gboolean
text_view_save(GtkTextView *text_view, GtkWidget *parent)
{
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(text_view);
	return text_buffer_save(buffer, parent);
}

void
text_view_search(GtkTextView *text_view,
		const gchar *text,
		gboolean forward)
{
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(text_view);
	GtkTextMark *mark = gtk_text_buffer_get_mark(buffer, "search");
	GtkTextIter iter;
	if (forward == FALSE) {
		if (mark == NULL)
			gtk_text_buffer_get_end_iter(buffer, &iter);
		else {
			gtk_text_buffer_get_iter_at_mark(buffer, &iter, mark);
			if (gtk_text_iter_backward_word_start(&iter) == FALSE)
				return;
		}
	} else {
		if (mark == NULL)
			gtk_text_buffer_get_start_iter(buffer, &iter);
		else
			gtk_text_buffer_get_iter_at_mark(buffer, &iter, mark);
	}
	text_buffer_search(buffer, &iter, text, forward);
	mark = gtk_text_buffer_get_mark(buffer, "search");
	if (mark)
		gtk_text_view_scroll_mark_onscreen(text_view, mark);
}

void
text_view_set_cursor(GtkTextView *text_view,
		gint x,
		gint y)
{
	static gboolean hovering_over_link = FALSE;

	GtkTextIter iter;
	gtk_text_view_get_iter_at_location(text_view, &iter, x, y);
	GSList *slist = gtk_text_iter_get_tags(&iter);
	gboolean hovering = FALSE;
	for (GSList *elem = slist; elem; elem = elem->next) {
		GtkTextTag *tag = elem->data;
		gchar *data = g_object_get_data(G_OBJECT(tag), "data");
		if (data) {
			hovering = TRUE;
			break;
		}
	}
	if (hovering != hovering_over_link) {
		hovering_over_link = hovering;
		GdkWindow *window = gtk_text_view_get_window(text_view,
				GTK_TEXT_WINDOW_TEXT);
		if (hovering_over_link)
			gdk_window_set_cursor(window, hand_cursor);
		else
			gdk_window_set_cursor(window, regular_cursor);
	}
	if (slist)
		g_slist_free(slist);
}

void
text_view_insert_hash_bucket(gpointer key,
		gpointer data,
		gpointer user_data)
{

	GtkTextBuffer *buffer =
		gtk_text_view_get_buffer(GTK_TEXT_VIEW(user_data));
	GtkTextIter iter;
	gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(buffer), &iter);
	text_buffer_insert_header(buffer, &iter, (gchar *)key);
	text_buffer_insert_slist(buffer, &iter, (GSList *)data);
}

void
text_view_markup(GtkWidget *text_view)
{
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
	GtkTextIter start, end;
	gtk_text_buffer_get_bounds(buffer, &start, &end);
	text_buffer_linkify(buffer, start, end);
}

void
text_view_update_cursor(GtkWidget *text_view)
{
	gint wx, wy;
	gdk_window_get_pointer(text_view->window, &wx, &wy, NULL);
	gint bx, by;
	gtk_text_view_window_to_buffer_coords(GTK_TEXT_VIEW(text_view),
			GTK_TEXT_WINDOW_WIDGET, wx, wy, &bx, &by);
	text_view_set_cursor(GTK_TEXT_VIEW(text_view), bx, by);
}

gboolean
tree_model_append(GtkTreeModel *model,
		GtkTreeIter *itera,
		gint column,
		const gchar *word,
		gboolean unique)
{
	gchar *new_word = g_strstrip(g_strdup(word));
	GtkTreeIter iter;
	if (unique && gtk_tree_model_get_iter_first(model, &iter))
		do {
			gchar *str;
			gtk_tree_model_get(model, &iter, column, &str, -1);
			if (str && g_ascii_strcasecmp(str, word) == 0) {
				g_free(str);
				return FALSE;
			}
			g_free(str);
		} while (gtk_tree_model_iter_next(model, &iter));
	GtkListStore *store = GTK_LIST_STORE(model);
	gtk_list_store_append(store, &iter);
	gtk_list_store_set(store, &iter, column, word, -1);
	g_free(new_word);
	if (itera)
		*itera = iter;

	return TRUE;
}

void
tree_model_clear(GtkTreeModel *model)
{
	GtkListStore *store = GTK_LIST_STORE(model);
	gtk_list_store_clear(store);
}

gboolean
tree_model_iter_prev(GtkTreeModel *model,
		GtkTreeIter *iter)
{
	GtkTreePath *path = gtk_tree_model_get_path(model, iter);
	if (gtk_tree_path_prev(path) == FALSE)
		return FALSE;
	(void)gtk_tree_model_get_iter(model, iter, path);
	gtk_tree_path_free(path);

	return TRUE;
}

gboolean
tree_view_button_press(GtkWidget *parent,
		GdkEventButton *event,
		gpointer user_data)
{
	if (event->type == GDK_BUTTON_PRESS && event->button == 3) {
		tree_view_menu_popup(parent, event, NULL);
		return TRUE;
	}

	return FALSE;
}

void
tree_view_clear(GtkTreeView *tree_view)
{
	GtkTreeModel *model = gtk_tree_view_get_model(tree_view);
	GtkTreeStore *store = GTK_TREE_STORE(model);
	gtk_tree_store_clear(store);
}

void
tree_view_collapse_all(GtkWidget *widget,
		gpointer data)
{
	gtk_tree_view_collapse_all(GTK_TREE_VIEW(data));
}

GtkWidget *
tree_view_new(const gchar **columns,
		guint n_columns)
{
	GtkWidget *tree_view = gtk_tree_view_new();
	GtkCellRenderer *cell = gtk_cell_renderer_text_new();
	for (guint i = 0; i < n_columns; i++) {
		GtkTreeViewColumn *column = gtk_tree_view_column_new_with_attributes(columns[i],
				cell, "text", i, NULL);
		gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view), column);
	}
	GtkTreeStore *store = gtk_tree_store_new(n_columns, G_TYPE_STRING, G_TYPE_STRING);
	gtk_tree_view_set_model(GTK_TREE_VIEW(tree_view),
			GTK_TREE_MODEL(store));
	g_object_unref(store);
	gtk_tree_view_set_enable_tree_lines(GTK_TREE_VIEW(tree_view), TRUE);
	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view));
	gtk_tree_selection_set_select_function(selection,
			tree_view_selection_func, NULL, NULL);
	g_object_connect(GTK_OBJECT(tree_view),
			"signal::button-press-event",
			G_CALLBACK(tree_view_button_press),
			NULL,
			"signal::popup-menu",
			G_CALLBACK(tree_view_menu_popup), NULL,
			NULL);

	return tree_view;
}

void
tree_view_expand_all(GtkWidget *widget,
		gpointer data)
{
	gtk_tree_view_expand_all(GTK_TREE_VIEW(data));
}

gchar *
tree_view_get_selection(GtkWidget *widget,
		gint column)
{
	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(widget));
	GtkTreeModel *model = GTK_TREE_MODEL(gtk_tree_view_get_model(GTK_TREE_VIEW(widget)));
	gchar *name = NULL;
	GtkTreeIter iter;
	if (gtk_tree_selection_get_selected(selection, &model, &iter))
			gtk_tree_model_get(model, &iter, column, &name, -1); 

	return name;
}

void
tree_view_insert(gpointer key,
		gpointer data,
		gpointer user_data)
{
	tree_view_insert_list(GTK_TREE_VIEW(user_data), (gchar *)key, (GSList *)data);
}

void
tree_view_insert_list(GtkTreeView *tree_view,
		const gchar *header,
		GSList *slist)
{
	GtkTreeStore *store = GTK_TREE_STORE(gtk_tree_view_get_model(tree_view));
	GtkTreeIter iter;
	gtk_tree_store_append(store, &iter, NULL);
	guint length = g_slist_length(slist);
	gchar *str = g_strdup_printf("%s (%d)", header, length);
	gtk_tree_store_set(store, &iter, 0, str, -1);
	g_free(str);
	for (const GSList *elem = slist; elem; elem = elem->next) {
		GtkTreeIter child;
		gtk_tree_store_append(store, &child, &iter);
		gtk_tree_store_set(store, &child, 0, (gchar *)elem->data, -1);
	}
}

void
tree_view_menu_popup(GtkWidget *parent,
		GdkEventButton *event_button,
		gpointer user_data)
{
	menu_popup(parent, event_button, "TreeViewActions",
			"/TreeViewMenu", menu_popup_entries,
			G_N_ELEMENTS(menu_popup_entries),
			menu_popup_entries_info);
}

gboolean
tree_view_selection_func(GtkTreeSelection *selection,
		GtkTreeModel *model,
		GtkTreePath *path,
		gboolean path_currently_selected,
		gpointer data)
{
	return (gtk_tree_path_get_depth(path) > 1);
}

GtkUIManager *
ui_manager_new(GtkWidget *parent,
		const gchar *name,
		const GtkActionEntry *entries,
		guint n_entries,
		const gchar *entries_info)
{
	GtkActionGroup *action_group = gtk_action_group_new(name);
	gtk_action_group_add_actions(action_group, entries, n_entries, parent);
	GtkUIManager *ui_manager = gtk_ui_manager_new();
	g_object_set_data_full(G_OBJECT(parent), "ui-manager", ui_manager,
			g_object_unref);
	gtk_ui_manager_insert_action_group(ui_manager, action_group, 0);
	if (GTK_IS_WINDOW(parent))
		gtk_window_add_accel_group(GTK_WINDOW(parent),
				gtk_ui_manager_get_accel_group(ui_manager));
	GError *error = NULL;
	if (!gtk_ui_manager_add_ui_from_string(ui_manager, entries_info, -1,
				&error))
		if (error) {
			g_warning("%s", error->message);
			g_error_free(error);
		}

	return ui_manager;
}

void
ui_manager_action_set_sensitive(GtkUIManager *ui_manager,
		const gchar *action_group_name,
		const gchar *action_name,
		gboolean sensitive)
{
	GList *list = gtk_ui_manager_get_action_groups(ui_manager);
	while (list) {
		GtkActionGroup *action_group = GTK_ACTION_GROUP(list->data);
		const gchar *name = gtk_action_group_get_name(action_group);
		if (g_strcmp0(name, action_group_name) == 0) {
			GtkAction *action = gtk_action_group_get_action(action_group, action_name);
			if (action) {
				gtk_action_set_sensitive(action, sensitive);
				break;
			}
		}
		list = list->next;
	}
}

void
ui_manager_widget_set_sensitive(GtkUIManager *ui_manager,
		const gchar *path,
		gboolean sensitive)
{
	GtkWidget *widget = gtk_ui_manager_get_widget(ui_manager, path);
	gtk_widget_set_sensitive(widget, sensitive);
}

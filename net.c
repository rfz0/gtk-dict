#include <glib.h>

#include "compat.h"
#include "net.h"

gboolean
net_close(net_t *nc,
		GError **error)
{
	gboolean status = g_io_stream_close(G_IO_STREAM(nc->connection),
			nc->cancellable,
			error);
	g_cancellable_reset((nc)->cancellable);
	g_object_unref(nc->ios.data_output);
	g_object_unref(nc->ios.output);
	g_object_unref(nc->ios.data_input);
	g_object_unref(nc->ios.input);

	return status;
}

void
net_destroy(net_t *nc)
{
	if (nc->host)
		g_free(nc->host);
	g_free(nc);
}

net_t *
net_new(const char *host,
		guint16 port,
		guint timeout,
		GDataStreamNewlineType newline_type)
{
	net_t *nc = g_new0(net_t, 1);
	nc->cancellable = g_cancellable_new();
	nc->host = g_strdup(host);
	nc->port = port;
	nc->timeout = timeout;
	nc->ios.newline_type = newline_type;

	return nc;
}

gboolean
net_open(net_t *nc,
		GError **error)
{
	GSocketClient *client = g_socket_client_new();
	g_socket_client_set_timeout(client, nc->timeout);
	nc->connection = g_socket_client_connect_to_host(client,
			nc->host, nc->port, nc->cancellable,
			error);
	g_object_unref(client);
	g_cancellable_reset((nc)->cancellable);
	if (nc->connection == NULL)
		return FALSE;
	nc->ios.input = g_io_stream_get_input_stream(G_IO_STREAM(nc->connection));
	nc->ios.data_input = g_data_input_stream_new(nc->ios.input);
	g_data_input_stream_set_newline_type(
			G_DATA_INPUT_STREAM(nc->ios.data_input),
			nc->ios.newline_type);
	nc->ios.output = g_io_stream_get_output_stream(G_IO_STREAM(nc->connection));
	nc->ios.data_output = g_data_output_stream_new(nc->ios.output);
	nc->ios.newline = "\r\n";

	return TRUE;
}

gboolean
net_read_line(net_t *nc,
		gchar **data,
		gsize *length,
		GError **error)
{
	*data = g_data_input_stream_read_line(nc->ios.data_input,
			length, nc->cancellable, error);
	g_cancellable_reset((nc)->cancellable);
	if (*data == NULL)
		return FALSE;

	return TRUE;
}

const gchar *
net_get_host(net_t *nc)
{
	return nc->host;
}

void
net_set_host(net_t *nc, const gchar *host)
{
	if (nc->host)
		g_free(nc->host);
	nc->host = g_strdup(host);
}

gboolean
net_write(net_t *nc,
		const gchar *buffer,
		GError **error)
{
	GString *str = g_string_new(buffer);
	g_string_append(str, nc->ios.newline);
	gboolean status = TRUE;
	gsize bytes_written;
	do {
		if (g_output_stream_write_all(nc->ios.output,
					str->str,
					str->len,
					&bytes_written,
					nc->cancellable,
					error) ==
				FALSE) {
			status = FALSE;
			break;
		}
	} while (bytes_written != str->len);
	g_cancellable_reset((nc)->cancellable);
	g_string_free(str, TRUE);

	return status;
}

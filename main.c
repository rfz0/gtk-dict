#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <gtk/gtk.h>

#include "app.h"

static GOptionEntry entries[] = { {NULL} };

int
main(int argc, char *argv[])
{
	GOptionContext *context = g_option_context_new("");
	g_option_context_add_main_entries(context, entries, NULL);
	g_option_context_add_group(context, gtk_get_option_group(TRUE));
	GError *error = NULL;
	if (g_option_context_parse(context, &argc, &argv, &error) == FALSE) {
		if (error) {
			g_warning("%s", error->message);
			g_error_free(error);
		}
		exit(EXIT_FAILURE);
	}
	g_option_context_free(context);

	g_type_init();
	g_thread_init(NULL);
	gdk_threads_init();
	gdk_threads_enter();
	gtk_init(&argc, &argv);

	app_create();
	app_init();

	gtk_main();
	
	gdk_threads_leave();

	app_destroy();

        return 0;
}

#include <gtk/gtk.h>

#include "app.h"
#include "compat.h"
#include "dict.h"
#include "gui.h"
#include "hash.h"
#include "hist.h"
#include "icon.h"

typedef void (*update_func_t)(GSList *, dict_cmd_t);
typedef void (*combo_box_update_func_t)(dict_t *, gchar *);

typedef struct {
	dict_req_t *dict_req;
	gboolean hist_save;
} app_req_t;

static void app_req_destroy(app_req_t *);
static app_req_t *app_req_new(dict_cmd_t, gchar *, gboolean);
static void backward(void);
static void cancel(void);
static void clear(void);
static void combo_box_changed(GtkComboBoxText *, gpointer);
static void define(GtkWidget *, GtkEntry *);
static GHashTable *define_format(GSList *);
static void define_update(GSList *, dict_cmd_t);
static void enable_cancel(gboolean);
static void enable_history(gboolean sensitive);
static void enable_history_backward(gboolean);
static void enable_history_clear(gboolean);
static void enable_history_forward(gboolean);
static void enable_input(gboolean);
static void enable_save(gboolean);
static void find_toggle(void);
static void follow_link(gpointer data);
static void forward(void);
static void init_gui(void);
static void match(GtkWidget *, gpointer);
static void match_update(GSList *, dict_cmd_t);
static void req_async(app_req_t *);
static gboolean req_finish(app_req_t *);
static void req_save(app_req_t *);
static gpointer req_start(app_req_t *req);
static void req_update(app_req_t *req);
static void save(void);
static void search(GtkWidget *, gpointer);
static void set_host(void);
static void show_databases(void);
static void combo_box_format(GSList *slist);
static void combo_box_update(GSList *, dict_cmd_t);
static void show_server(void);
static void show_server_update(GSList *, dict_cmd_t);
static void show_strat(void);
static void text_buffer_changed(GtkWidget *, gpointer);
static void tree_view_selection_changed(GtkWidget *, gpointer);

static GStaticMutex mutex;
static dict_t *dict;
static hist_t *hist;
static GtkUIManager *ui_manager;
static GtkWidget *button_define, *button_highlight_all, *button_match,
	  *combo_box_database, *combo_box_strategy, *entry, *entry2,
	  *entry2_hbox, *statusbar, *text_view, *tree_view, *window;

static GtkActionEntry
ui_entries[] = {
	{ "FileMenu", NULL, "_File", NULL, NULL, NULL },
	{ "EditMenu", NULL, "_Edit", NULL, NULL, NULL },
	{ "HistoryMenu", NULL, "History", NULL, NULL, NULL },
	{ "ServerMenu", NULL, "_Server", NULL, NULL, NULL },
	{ "HelpMenu", NULL, "_Help", NULL, NULL, NULL },
	{ "Save", GTK_STOCK_SAVE, "_Save", "<control>S", "Save", G_CALLBACK(save) },
	{ "Quit", GTK_STOCK_QUIT, "_Quit", "<control>Q", "Quit", G_CALLBACK(gtk_main_quit) },
	{ "Find", GTK_STOCK_FIND, "_Find", "<control>F", "Find", G_CALLBACK(find_toggle)},
	{ "Clear", GTK_STOCK_DELETE, "_Clear", NULL, "Clear", G_CALLBACK(clear) },
	{ "Backward", GTK_STOCK_GO_BACK, "_Backward", NULL, "Backward", G_CALLBACK(backward) },
	{ "Forward", GTK_STOCK_GO_FORWARD, "_Forward", NULL, "Forward", G_CALLBACK(forward) },
	{ "Strategies", GTK_STOCK_REFRESH, "_Refresh Strategies", NULL, "Strategies", G_CALLBACK(show_strat) },
	{ "Host", NULL, "_Host", "<control>H", "Host", G_CALLBACK(set_host) },
	{ "Info", GTK_STOCK_INFO, "_Info", "<control>I", "Info", G_CALLBACK(show_server) },
	{ "Databases", GTK_STOCK_REFRESH, "Refresh _Databases", NULL, "Databases", G_CALLBACK(show_databases) },
	{ "About", GTK_STOCK_ABOUT, "_About", NULL, "About", G_CALLBACK(dialog_about) },
	{ "Cancel", GTK_STOCK_CANCEL, "_Cancel", NULL, "Cancel", G_CALLBACK(cancel) }
};

static const gchar *
ui_entries_info =
"<ui>"
" <menubar name='MenuBar'>"
"  <menu action='FileMenu'>"
"   <menuitem action='Save'/>"
"   <menuitem action='Quit'/>"
"  </menu>"
"  <menu action='EditMenu'>"
"   <menuitem action='Find'/>"
"  </menu>"
"  <menu action='HistoryMenu'>"
"   <menuitem action='Clear'/>"
"   <menuitem action='Backward'/>"
"   <menuitem action='Forward'/>"
"  </menu>"
"  <menu action='ServerMenu'>"
"   <menuitem action='Host'/>"
"   <menuitem action='Info'/>"
"   <menuitem action='Strategies'/>"
"   <menuitem action='Databases'/>"
"  </menu>"
"  <menu action='HelpMenu'>"
"   <menuitem action='About'/>"
"  </menu>"
" </menubar>"
" <toolbar name='ToolBar'>"
"  <toolitem action='Save'/>"
"  <toolitem action='Find'/>"
"  <toolitem action='Cancel'/>"
"  <toolitem action='Backward'/>"
"  <toolitem action='Forward'/>"
"  <toolitem action='Quit'/>"
" </toolbar>"
"</ui>";

static const gchar *
tree_view_column_headers[] = {
	"Matches",
};

static update_func_t
update[N_DICT_CMD] = {
	[DICT_CMD_AUTH] = NULL,
	[DICT_CMD_CLIENT] = NULL,
	[DICT_CMD_DEFINE] = define_update,
	[DICT_CMD_HELP] = NULL,
	[DICT_CMD_MATCH] = match_update,
	[DICT_CMD_OPTION] = NULL,
	[DICT_CMD_QUIT] = NULL,
	[DICT_CMD_SASLAUTH] = NULL,
	[DICT_CMD_SHOW_DATABASES] = combo_box_update,
	[DICT_CMD_SHOW_INFO] = NULL,
	[DICT_CMD_SHOW_SERVER] = show_server_update,
	[DICT_CMD_SHOW_STRAT] = combo_box_update,
	[DICT_CMD_STATUS] = NULL
};

void
app_create(void)
{
	hist = history_new((GDestroyNotify)dict_req_destroy);
	dict = dict_new();
}

void
app_destroy(void)
{
	dict_destroy(dict);
	history_destroy(hist);
}

void
app_init(void)
{
	init_gui();
	enable_history(FALSE);
	gtk_widget_show_all(window);
	gtk_widget_hide(entry2_hbox);
	req_async(app_req_new(DICT_CMD_SHOW_STRAT, NULL, FALSE));
	req_async(app_req_new(DICT_CMD_SHOW_DATABASES, NULL, FALSE));
}

void
app_req_destroy(app_req_t *req)
{
	if (req == NULL)
		return;
	dict_req_destroy(req->dict_req);
}

app_req_t *
app_req_new(dict_cmd_t cmd, gchar *arg, gboolean hist_save)
{
	app_req_t *req = g_new0(app_req_t, 1);
	req->dict_req = dict_req_new(cmd, arg);
	req->hist_save = hist_save;

	return req;
}

void 
backward(void)
{
	if (history_backward(hist)) {
		enable_history_backward(FALSE);
		enable_history_forward(TRUE);
		req_async((app_req_t *)history_data(hist));
	}
}

void
cancel(void)
{
	enable_cancel(FALSE);
	dict_cmd_cancel(dict);
}

void 
clear(void)
{
	enable_history(FALSE);
	history_clear(hist);
	gtk_entry_set_text(GTK_ENTRY(entry), "");
	entry_completion_clear(entry);
	gtk_entry_set_text(GTK_ENTRY(entry2), "");
	entry_completion_clear(entry2);
	text_view_clear(GTK_TEXT_VIEW(text_view));
	tree_view_clear(GTK_TREE_VIEW(tree_view));
	enable_save(FALSE);
}

void
combo_box_changed(GtkComboBoxText *combo_box,
		gpointer user_data)
{
	gchar *active_text = gtk_combo_box_text_get_active_text(combo_box);
	if (active_text) {
		combo_box_update_func_t func = user_data;
		(*func)(dict, active_text);
		g_free(active_text);
	}
}

void
combo_box_format(GSList *slist)
{
	for (GSList *elem = slist; elem; elem = elem->next) {
		gchar **match = g_strsplit((gchar *)elem->data, " ", 2);
		g_free(elem->data);
		elem->data = g_strdup(match[0]);
		/* match[0] is used as GtkComboBox menu item data.
		 * match[1] could be used as tooltip data but
		 * GtkComboBox doesn't support per menu item tooltips so
		 * we just discard it.
		 */
		g_strfreev(match);
	}
}

void
combo_box_update(GSList *slist, dict_cmd_t cmd)
{
	GtkWidget *combo_box;
	if (cmd == DICT_CMD_SHOW_DATABASES)
		combo_box = combo_box_database;
	else
		combo_box = combo_box_strategy;
	gdk_threads_enter();
	gint active = gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box));
	gtk_combo_box_text_remove_all(GTK_COMBO_BOX_TEXT(combo_box));
	combo_box_format(slist);
	g_slist_foreach(slist, combo_box_insert, combo_box);
	gtk_combo_box_text_prepend_text(GTK_COMBO_BOX_TEXT(combo_box),
			"default");
	gtk_combo_box_set_active(GTK_COMBO_BOX(combo_box),
			active == -1 ? 0 : active);
	gdk_flush();
	gdk_threads_leave();
}

void
define(GtkWidget *widget,
		GtkEntry *entry)
{
	const gchar *text = gtk_entry_get_text(entry);
	if (text == NULL)
		return;
	gchar *new_text = g_utf8_strdown(text, g_utf8_strlen(text, -1));
	g_strstrip(new_text);
	req_async(app_req_new(DICT_CMD_DEFINE, new_text, TRUE));
}

GHashTable *
define_format(GSList *slist)
{
	GHashTable *hash_table = hash_table_new();
	gchar *str = NULL;
	for (GSList *elem = slist; elem; elem = elem->next) {
		if (dict_cmd_response_code(elem->data) ==
				DICT_RESPONSE_151_WORD_DATABASE_NAME) {
			if (str)
				g_free(str);
			gchar **match = g_strsplit(elem->data, " ", 2);
			str = g_strdup(match[1]);
			g_strfreev(match);
			continue;
		}
		if (str)
			hash_table_prepend(hash_table, str, elem->data);
	}
	hash_table_reverse(hash_table);

	return hash_table;
}

void
define_update(GSList *slist, dict_cmd_t cmd)
{
	GHashTable *hash_table = define_format(slist);
	gdk_threads_enter();
	text_view_clear(GTK_TEXT_VIEW(text_view));
	g_hash_table_foreach(hash_table, text_view_insert_hash_bucket, text_view);
	text_view_markup(text_view);
	gdk_flush();
	gdk_threads_leave();
	hash_table_destroy(hash_table);
}

void
enable_cancel(gboolean sensitive)
{
	ui_manager_widget_set_sensitive(ui_manager, "/ToolBar/Cancel", sensitive);
}

void
enable_history(gboolean sensitive)
{
	enable_history_clear(sensitive);
	enable_history_forward(sensitive);
	enable_history_backward(sensitive);
}

void
enable_history_backward(gboolean sensitive)
{
	if (sensitive && (history_is_empty(hist) ||
				history_at_head(hist)))
			return;
	ui_manager_action_set_sensitive(ui_manager, "Actions",
			"Backward", sensitive);
	ui_manager_widget_set_sensitive(ui_manager, "/ToolBar/Backward", sensitive);
}

void
enable_history_clear(gboolean sensitive)
{
	if (sensitive && history_is_empty(hist))
		return;
	ui_manager_action_set_sensitive(ui_manager, "Actions", "Clear", sensitive);
}

void
enable_history_forward(gboolean sensitive)
{
	if (sensitive && (history_is_empty(hist) ||
				history_at_tail(hist)))
			return;
	ui_manager_action_set_sensitive(ui_manager, "Actions",
			"Forward", sensitive);
	ui_manager_widget_set_sensitive(ui_manager,
			"/ToolBar/Forward", sensitive);
}

void
enable_input(gboolean sensitive)
{
	gtk_widget_set_sensitive(entry, sensitive);
	gtk_widget_set_sensitive(tree_view, sensitive);
	gtk_widget_set_sensitive(text_view, sensitive);
	enable_history(sensitive);
	if (sensitive == FALSE) {
		gtk_widget_set_sensitive(button_define, FALSE);
		gtk_widget_set_sensitive(button_match, FALSE);
	} else {
		guint length;
		g_object_get(entry, "text-length", &length, NULL);
		if (length > 0) {
			gtk_widget_set_sensitive(button_define, TRUE);
			gtk_widget_set_sensitive(button_match, TRUE);
		}
	}
}

void
enable_save(gboolean sensitive)
{
	ui_manager_action_set_sensitive(ui_manager, "Actions", "Save", sensitive);
	ui_manager_widget_set_sensitive(ui_manager, "/ToolBar/Save", sensitive);
}

void
find_toggle(void)
{
	if (GTK_WIDGET_VISIBLE(entry2_hbox))
		gtk_widget_hide(entry2_hbox);
	else {
		gtk_widget_show(entry2_hbox);
		gtk_widget_grab_focus(entry2);
	}
}

void
follow_link(gpointer data)
{
	gchar *str = (gchar *)data;
	gchar *newstr = g_utf8_strdown(str, g_utf8_strlen(str, -1));
	req_async(app_req_new(DICT_CMD_DEFINE, newstr, TRUE));
}

void 
forward(void)
{
	if (history_forward(hist)) {
		enable_history_backward(TRUE);
		enable_history_forward(FALSE);
		req_async((app_req_t *)history_data(hist));
	}
}

void
init_gui(void)
{
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_usize(window, 480, 320);
	gtk_window_set_title(GTK_WINDOW(window), TARGET);
	g_signal_connect(GTK_OBJECT(window), "delete_event", gtk_main_quit,
			NULL);

	GdkPixbuf *pixbuf = pixbuf_from_inline(icon);
	if (pixbuf) {
		gtk_window_set_icon(GTK_WINDOW(window), pixbuf);
		g_object_unref(G_OBJECT(pixbuf));
	}

	GtkWidget *vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);

	ui_manager = ui_manager_new(window,
			"Actions",
			ui_entries,
			G_N_ELEMENTS(ui_entries),
			ui_entries_info);

	GtkWidget *menu_bar = gtk_ui_manager_get_widget(ui_manager, "/MenuBar");
	gtk_box_pack_start(GTK_BOX(vbox), menu_bar, FALSE, FALSE,
			0);

	GtkWidget *tool_bar = gtk_ui_manager_get_widget(ui_manager, "/ToolBar");
	gtk_box_pack_start(GTK_BOX(vbox), tool_bar, FALSE, FALSE,
			0);

	enable_cancel(FALSE);
	enable_save(FALSE);

	GtkWidget *entry_hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), entry_hbox, FALSE,
			FALSE, 0);

	entry = entry_new(DICT_CMD_RESTRICT_CHARSET);
	g_signal_connect(GTK_OBJECT(entry), "activate",
			G_CALLBACK(define), entry);

	GtkWidget *label_entry = gtk_label_new("Search:");
	gtk_box_pack_start(GTK_BOX(entry_hbox), label_entry, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(entry_hbox), entry, TRUE, TRUE, 0);

	combo_box_database = gtk_combo_box_text_new();
	g_signal_connect(GTK_OBJECT(combo_box_database),
			"changed",
			G_CALLBACK(combo_box_changed),
			dict_set_db);
	gtk_widget_set_tooltip_text(combo_box_database, "database");
	gtk_box_pack_start(GTK_BOX(entry_hbox), combo_box_database,
			FALSE, FALSE, 0);

	combo_box_strategy = gtk_combo_box_text_new();
	g_signal_connect(GTK_OBJECT(combo_box_strategy),
			"changed",
			G_CALLBACK(combo_box_changed),
			dict_set_strat);
	gtk_widget_set_tooltip_text(combo_box_strategy, "strategy");
	gtk_box_pack_start(GTK_BOX(entry_hbox),
			combo_box_strategy, FALSE, FALSE, 0);

	button_define = gtk_button_new_with_mnemonic("_define");
	g_signal_connect(GTK_OBJECT(button_define), "clicked",
			G_CALLBACK(define), entry);
	gtk_widget_set_sensitive(button_define, FALSE);
	gtk_box_pack_start(GTK_BOX(entry_hbox), button_define,
			FALSE, FALSE, 0);
	g_signal_connect(GTK_OBJECT(entry), "changed",
			G_CALLBACK(entry_changed),
			button_define);

	button_match = gtk_button_new_with_mnemonic("_match");
	g_signal_connect(GTK_OBJECT(button_match), "clicked",
			G_CALLBACK(match), entry);
	gtk_widget_set_sensitive(button_match, FALSE);
	gtk_box_pack_start(GTK_BOX(entry_hbox), button_match,
			FALSE, FALSE, 0);
	g_signal_connect(GTK_OBJECT(entry), "changed",
			G_CALLBACK(entry_changed),
			button_match);

	GtkWidget *hpaned = gtk_hpaned_new();
	gtk_box_pack_start(GTK_BOX(vbox), hpaned, TRUE, TRUE, 0);

	GtkWidget *frame_left = gtk_frame_new(NULL);
	gtk_paned_pack1(GTK_PANED(hpaned), frame_left, TRUE,
			TRUE);
	gtk_frame_set_shadow_type(GTK_FRAME(frame_left),
			GTK_SHADOW_IN);

	GtkWidget *frame_left_vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(frame_left),
			frame_left_vbox);

	GtkWidget *scrolled_window_left = gtk_scrolled_window_new(NULL, NULL);
	gtk_box_pack_start(GTK_BOX(frame_left_vbox),
			scrolled_window_left, TRUE, TRUE, 0);
	text_view = text_view_new(follow_link);
	gtk_container_add(GTK_CONTAINER(scrolled_window_left),
			text_view);

	GtkTextBuffer *text_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
	g_signal_connect(G_OBJECT(text_buffer), "changed",
			G_CALLBACK(text_buffer_changed), NULL);

	entry2_hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(frame_left_vbox),
			entry2_hbox, FALSE, FALSE, 0);

	GtkWidget *button_find_close = gtk_button_new();
	gtk_button_set_image(GTK_BUTTON(button_find_close),
			gtk_image_new_from_stock(GTK_STOCK_CLOSE,
				GTK_ICON_SIZE_BUTTON));
	g_signal_connect(GTK_OBJECT(button_find_close), "clicked",
			G_CALLBACK(find_toggle), entry2_hbox);
	gtk_box_pack_start(GTK_BOX(entry2_hbox),
			button_find_close, FALSE, FALSE, 0);

	entry2 = entry_new(NULL);
	g_signal_connect(GTK_OBJECT(entry2), "activate",
			G_CALLBACK(search), GINT_TO_POINTER(TRUE));
	g_object_set_data(G_OBJECT(text_view), "entry",
			entry2);

	GtkWidget *label_entry2 = gtk_label_new("Search:");
	gtk_box_pack_start(GTK_BOX(entry2_hbox), label_entry2,
			FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(entry2_hbox), entry2, TRUE,
			TRUE, 0);

	GtkWidget *button_highlight_next = gtk_button_new_with_mnemonic("_next");
	gtk_box_pack_start(GTK_BOX(entry2_hbox),
			button_highlight_next, FALSE, FALSE, 0);
	g_signal_connect(GTK_OBJECT(button_highlight_next), "clicked",
			G_CALLBACK(search),
			GINT_TO_POINTER(TRUE));
	gtk_widget_set_sensitive(button_highlight_next, FALSE);
	g_signal_connect(GTK_OBJECT(entry2), "changed",
			G_CALLBACK(entry_changed),
			button_highlight_next);

	GtkWidget *button_highlight_prev = gtk_button_new_with_mnemonic("_previous");
	gtk_box_pack_start(GTK_BOX(entry2_hbox),
			button_highlight_prev, FALSE, FALSE, 0);
	g_signal_connect(GTK_OBJECT(button_highlight_prev), "clicked",
			G_CALLBACK(search),
			GINT_TO_POINTER(FALSE));
	gtk_widget_set_sensitive(button_highlight_prev, FALSE);
	g_signal_connect(GTK_OBJECT(entry2), "changed",
			G_CALLBACK(entry_changed),
			button_highlight_prev);

	button_highlight_all =
		gtk_toggle_button_new_with_mnemonic("_all");
	gtk_box_pack_start(GTK_BOX(entry2_hbox),
			button_highlight_all, FALSE, FALSE, 0);
	gtk_widget_set_sensitive(button_highlight_all, FALSE);
	g_signal_connect(GTK_OBJECT(button_highlight_all), "toggled",
			G_CALLBACK(text_view_highlight_toggle),
			text_view);
	g_signal_connect(GTK_OBJECT(entry2), "changed",
			G_CALLBACK(entry_changed),
			button_highlight_all);

	GtkWidget *frame_right = gtk_frame_new(NULL);
	gtk_paned_pack2(GTK_PANED(hpaned), frame_right, FALSE,
			TRUE);
	gtk_frame_set_shadow_type(GTK_FRAME(frame_right),
			GTK_SHADOW_IN);
	gtk_widget_set_size_request(frame_right, 100, -1);

	GtkWidget *frame_right_vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(frame_right),
			frame_right_vbox);

	GtkWidget *scrolled_window_right = gtk_scrolled_window_new(NULL,
			NULL);
	gtk_box_pack_start(GTK_BOX(frame_right_vbox),
			scrolled_window_right, TRUE, TRUE, 0);
	
	tree_view = tree_view_new(tree_view_column_headers,
			G_N_ELEMENTS(tree_view_column_headers));
	g_signal_connect(GTK_OBJECT(tree_view), "row-activated",
			G_CALLBACK(tree_view_selection_changed),
			NULL);
	gtk_container_add(GTK_CONTAINER(scrolled_window_right),
			tree_view);

	statusbar = gtk_statusbar_new();
	gtk_box_pack_end(GTK_BOX(vbox), statusbar, FALSE,
			FALSE, 0);

	gtk_widget_set_can_default(button_match, TRUE);
	gtk_widget_grab_default(button_match);

	GtkSettings *settings = gtk_settings_get_default();
	gtk_settings_set_long_property(settings, "gtk_tooltip_timeout", 0, NULL);
}

void
match(GtkWidget *widget,
		gpointer data)
{
	GtkWidget *entry = GTK_WIDGET(data);
	const gchar *text = gtk_entry_get_text(GTK_ENTRY(entry));
	req_async(app_req_new(DICT_CMD_MATCH, g_strdup(text), FALSE));
}

GHashTable *
match_format(GSList *slist)
{
	GHashTable *hash_table = hash_table_new();
	GSList *elem;
	for (elem = slist; elem; elem = elem->next) {
		gchar **match = g_strsplit(elem->data, " ", 2);
		if (match[1]) {
			match[1] = g_strdelimit(match[1], "\"", ' ');
			g_strstrip(match[1]);
			hash_table_prepend(hash_table, match[0],
					match[1]);
		}
		g_strfreev(match);
	}
	hash_table_reverse(hash_table);

	return hash_table;
}

void
match_update(GSList *slist, dict_cmd_t cmd)
{
	GHashTable *hash_table = match_format(slist);
	gdk_threads_enter();
	tree_view_clear(GTK_TREE_VIEW(tree_view));
	g_hash_table_foreach(hash_table, tree_view_insert,
			tree_view);
	gdk_flush();
	gdk_threads_leave();
	hash_table_destroy(hash_table);
}

void
req_async(app_req_t *req)
{
	GError *error = NULL;
	GThread *thread = g_thread_create((GThreadFunc)req_start, req, FALSE, &error);
	if (thread == NULL) {
		if (error) {
			g_warning("%s", error->message);
			g_error_free(error);
		}
	}
}

gboolean
req_finish(app_req_t *app_req)
{
	gdk_threads_enter();
	enable_cancel(FALSE);
	gdk_flush();
	gdk_threads_leave();
	dict_cancel_clear(dict);
	req_update(app_req);
	req_save(app_req);
	gdk_threads_enter();
	enable_input(TRUE);
	gtk_widget_grab_focus(entry);
	gdk_flush();
	gdk_threads_leave();
	g_static_mutex_unlock(&mutex);

	/* This must return false so g_idle_add removes it from the list
	 * of event sources and does not call it again. */
	return FALSE;
}

void
req_save(app_req_t *app_req)
{
	dict_req_t *dict_req = app_req->dict_req;
	if (dict_req->cmd != DICT_CMD_DEFINE || app_req->hist_save == FALSE)
		return;
	if (history_at_head(hist) == FALSE) {
		app_req_t *last_app_req = (app_req_t *)history_data(hist);
		if (last_app_req) {
			dict_req_t *last_dict_req = last_app_req->dict_req;
			if (last_dict_req && g_strcmp0(last_dict_req->arg,
						dict_req->arg) == 0) {
				app_req_destroy(app_req);
				return;
			}
		}
	}
	app_req->hist_save = FALSE;
	history_append(hist, app_req);
}

gpointer
req_start(app_req_t *req)
{
	g_static_mutex_lock(&mutex);
	gdk_threads_enter();
	enable_input(FALSE);
	enable_cancel(TRUE);
	gdk_flush();
	gdk_threads_leave();
	GError *error = NULL;
	statusbar_write(GTK_STATUSBAR(statusbar), "open");
	if (dict_open(dict, &error)) {
		statusbar_write(GTK_STATUSBAR(statusbar), "run");
		if (dict_cmd(dict, req->dict_req, &error)) {
			gdk_threads_enter();
			enable_cancel(TRUE);
			gdk_flush();
			gdk_threads_leave();
			statusbar_write(GTK_STATUSBAR(statusbar),
					"close");
			(void)dict_close(dict, &error);
		}
	}
	if (error) {
		g_warning("%s", error->message);
		statusbar_write(GTK_STATUSBAR(statusbar),
				error->message);
		g_error_free(error);
	} else
		statusbar_write(GTK_STATUSBAR(statusbar), "ready");
	g_idle_add((GSourceFunc)req_finish, req);

	return NULL;
}

void
req_update(app_req_t *app_req)
{
	dict_req_t *dict_req = app_req->dict_req;
	if (dict_req->res == NULL)
		return;
	if (update[dict_req->cmd])
		(*update[dict_req->cmd])(dict_req->res, dict_req->cmd);
	g_slist_free_full(dict_req->res, g_free);
	dict_req->res = NULL;
	gdk_threads_enter();
	if (dict_req->arg)
		entry_completion_append(GTK_ENTRY(entry),
				dict_req->arg);
	gdk_flush();
	gdk_threads_leave();
}

void
save(void)
{
	(void)text_view_save(GTK_TEXT_VIEW(text_view), window);
}

void
search(GtkWidget *widget,
		gpointer data)
{
	GtkEntry *entry = GTK_ENTRY(entry2);
	if (gtk_entry_get_text_length(entry) == 0)
		return;
	const gchar *text = gtk_entry_get_text(entry);
	gboolean forward = GPOINTER_TO_INT(data);
	text_view_search(GTK_TEXT_VIEW(text_view), text,
			forward);
	entry_completion_append(entry, text);
}

void
set_host(void)
{
	gchar *text = dialog_input("host", dict_get_host(dict));
	if (text) {
		dict_set_host(dict, text);
		g_free(text);
	}
}

void
show_databases(void)
{
	req_async(app_req_new(DICT_CMD_SHOW_DATABASES, NULL, FALSE));
}

void
show_server(void)
{
	req_async(app_req_new(DICT_CMD_SHOW_SERVER, NULL, FALSE));
}

void
show_server_update(GSList *slist, dict_cmd_t cmd)
{
	GString *str = g_string_new(NULL);
	for (GSList *elem = slist; elem; elem = elem->next) {
		(void)g_string_append(str, elem->data);
		(void)g_string_append_c(str, '\n');
	}
	gdk_threads_enter();
	dialog_info(window, "Server Information", str->str);
	gdk_flush();
	gdk_threads_leave();
	g_string_free(str, TRUE);
}

void
show_strat(void)
{
	req_async(app_req_new(DICT_CMD_SHOW_STRAT, NULL, FALSE));
}

void
text_buffer_changed(GtkWidget *widget,
		gpointer data)
{
	if (gtk_text_buffer_get_char_count(GTK_TEXT_BUFFER(widget)) > 0)
		enable_save(TRUE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button_highlight_all),
			FALSE);
}

void
tree_view_selection_changed(GtkWidget *widget,
		gpointer data)
{
	gchar *word = tree_view_get_selection(widget, 0);
	if (word)
		req_async(app_req_new(DICT_CMD_DEFINE, word, TRUE));
}

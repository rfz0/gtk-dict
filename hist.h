#ifndef HIST_H
#define HIST_H

#include <glib.h>

typedef struct {
	GDestroyNotify free_func;
	gint index;
	GSList *data;
} hist_t;

void history_append(hist_t *, gpointer);
gboolean history_at_head(hist_t *);
gboolean history_at_tail(hist_t *);
gboolean history_backward(hist_t *);
void history_clear(hist_t *);
gpointer history_data(hist_t *);
void history_destroy(hist_t *);
gboolean history_forward(hist_t *);
gboolean history_is_empty(hist_t *);
hist_t *history_new(GDestroyNotify);

#endif

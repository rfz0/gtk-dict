#ifndef COMPAT_H
#define COMPAT_H

#include <gtk/gtk.h>

/* XXX */

#if ! defined(g_slist_free_full)
#define g_slist_free_full(x, f) g_slist_foreach(x, (GFunc)f, NULL)
#endif

#if ! defined(GTK_COMBO_BOX_TEXT)
#define GTK_COMBO_BOX_TEXT GTK_COMBO_BOX
#define GtkComboBoxText GtkComboBox 
#define gtk_combo_box_text_append_text gtk_combo_box_append_text
#define gtk_combo_box_text_get_active_text gtk_combo_box_get_active_text
#define gtk_combo_box_text_new gtk_combo_box_new_text 
#define gtk_combo_box_text_prepend_text gtk_combo_box_prepend_text
#endif

#if ! defined(g_socket_client_set_timeout)
#define g_socket_client_set_timeout(x, n) do { (void)x,(void)n; } while(0)
#endif

#if ! defined(gtk_combo_box_text_remove_all)
#define gtk_combo_box_text_remove_all(x) {			\
	GtkTreeModel *store;					\
	store = gtk_combo_box_get_model(GTK_COMBO_BOX(x));	\
	gtk_list_store_clear(GTK_LIST_STORE(store));		\
}
#endif

#endif

#ifndef GUI_H
#define GUI_H

#include <glib.h>
#include <gtk/gtk.h>

void combo_box_insert(gpointer, gpointer);
void dialog_about(void);
void dialog_info(GtkWidget *, const gchar *, const gchar *);
gchar *dialog_input(const gchar *, const gchar *);
void dialog_license(void);
void dialog_save(GtkWindow *, gchar **);
void entry_changed(GtkEditable *, GtkWidget *);
void entry_clear(GtkWidget *, gpointer);
void entry_completion_append(GtkEntry *, const gchar *);
void entry_completion_clear(GtkWidget *);
GtkWidget *entry_new(gchar *);
GtkWidget *menu_new(GtkWidget *, const gchar *, const gchar *, const
		GtkActionEntry *, guint, const gchar *);
GdkPixbuf * pixbuf_from_inline(const guint8 *);
void statusbar_write(GtkStatusbar *, const gchar *, ...);
gboolean text_buffer_save(GtkTextBuffer *, GtkWidget *);
void text_view_clear(GtkTextView *);
GtkWidget *text_view_new(void (*)(gpointer));
void text_view_highlight_all(GtkTextView *);
void text_view_highlight_clear(GtkTextView *);
void text_view_highlight_toggle(GtkWidget *, gpointer);
void text_view_insert_hash_bucket(gpointer, gpointer, gpointer);
void text_view_markup(GtkWidget *);
gboolean text_view_save(GtkTextView *, GtkWidget *parent);
void text_view_search(GtkTextView *, const gchar *, gboolean);
void text_view_update_cursor(GtkWidget *);
gboolean tree_model_append(GtkTreeModel *, GtkTreeIter *, gint, const gchar *,
		gboolean);
void tree_model_clear(GtkTreeModel *);
gboolean tree_model_iter_prev(GtkTreeModel *, GtkTreeIter *);
gboolean tree_model_get_iter_last(GtkTreeModel *, GtkTreeIter *);
void tree_view_clear(GtkTreeView *);
GtkWidget *tree_view_new(const gchar **, guint);
void tree_view_insert(gpointer, gpointer, gpointer);
gchar *tree_view_get_selection(GtkWidget *, gint);
GtkUIManager *ui_manager_new(GtkWidget *, const gchar *,
		const GtkActionEntry *, guint, const gchar *);
void ui_manager_action_set_sensitive(GtkUIManager *, const gchar *, const gchar
		*, gboolean);
void ui_manager_widget_set_sensitive(GtkUIManager *, const gchar *, gboolean);

#endif

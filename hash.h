#ifndef HASH_H
#define HASH_H

#include <glib.h>

void hash_bucket_free(gpointer, gpointer);
void hash_bucket_reverse(gpointer, gpointer, gpointer);

#define hash_table_new()							\
	g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL)

#define hash_table_prepend(hash, key, value)					\
	g_hash_table_replace(hash, g_strdup(key),				\
			g_slist_prepend(g_hash_table_lookup(hash,		\
					key), g_strdup(value)))

#define hash_table_reverse(hash)						\
	g_hash_table_foreach(hash, hash_bucket_reverse, hash)

#define hash_table_destroy(hash) do {						\
		g_hash_table_foreach(hash, (GHFunc)hash_bucket_free, NULL);	\
		g_hash_table_destroy(hash);					\
	} while (0)

#endif

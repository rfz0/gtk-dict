#ifndef DICT_H
#define DICT_H

#include <glib.h>

#include "net.h"

#define DICT_RESPONSE_110_DATABASES_PRESENT 110
#define DICT_RESPONSE_111_STRATEGIES_AVAILABLE 111
#define DICT_RESPONSE_112_DATABASE_INFORMATION 112
#define DICT_RESPONSE_113_HELP_TEXT 113
#define DICT_RESPONSE_114_SERVER_INFORMATION 114
#define DICT_RESPONSE_130_CHALLENGE_FOLLOWS 130
#define DICT_RESPONSE_150_DEFINITIONS_RETRIEVED 150
#define DICT_RESPONSE_151_WORD_DATABASE_NAME 151
#define DICT_RESPONSE_152_MATCHES_FOUND 152
#define DICT_RESPONSE_210_STATISTICAL_INFORMATION 210
#define DICT_RESPONSE_220_TEXT_CAPABILITIES_MSGID 220
#define DICT_RESPONSE_221_CLOSING_CONNECTION 221
#define DICT_RESPONSE_230_AUTHENTICATION_SUCCESSFUL 230
#define DICT_RESPONSE_250_OK 250
#define DICT_RESPONSE_330_SEND_RESPONSE 330
#define DICT_RESPONSE_420_SERVER_TEMPORARILY_UNAVAILABLE 420
#define DICT_RESPONSE_421_SERVER_SHUTTING_DOWN 421
#define DICT_RESPONSE_500_SYNTAX_ERROR_COMMAND_NOT_RECOGNIZED 500
#define DICT_RESPONSE_501_SYNTAX_ERROR_ILLEGAL_PARAMETER 501
#define DICT_RESPONSE_502_COMMAND_NOT_IMPLEMENTED 502
#define DICT_RESPONSE_503_COMMAND_PARAMETER_NOT_IMPLEMENTED 503
#define DICT_RESPONSE_530_ACCESS_DENIED 530
#define DICT_RESPONSE_531_ACCESS_DENIED_USE_SHOW_INFO 531
#define DICT_RESPONSE_532_ACCESS_DENIED_UNKNOWN_MECHANISM 532
#define DICT_RESPONSE_550_INVALID_DATABASE 550
#define DICT_RESPONSE_551_INVALID_STRAT 551
#define DICT_RESPONSE_552_NO_MATCH 552
#define DICT_RESPONSE_554_NO_DATABASES_PRESENT 554
#define DICT_RESPONSE_555_NO_STRATEGIES_AVAILABLE 555

#define DICT_CMD_RESTRICT_CHARSET "'\"\\"

#define DICT_CMD_ARG_DATABASE_ALL "!"
#define DICT_CMD_ARG_DATABASE_ALL_ALL "*"
#define DICT_CMD_ARG_STRAT_EXACT "exact"
#define DICT_CMD_ARG_STRAT_PREFIX "prefix"
#define DICT_CMD_ARG_STRAT_SERVER "."
#define DICT_CMD_ARG_SHOW_DB "DB"
#define DICT_CMD_ARG_SHOW_STRAT "STRAT"
#define DICT_CMD_ARG_SHOW_INFO "INFO"
#define DICT_CMD_ARG_SHOW_SERVER "SERVER"

#define DICT_EOT "."

typedef enum {
	DICT_CMD_CANCEL = -1,
	DICT_CMD_AUTH,
	DICT_CMD_CLIENT,
	DICT_CMD_DEFINE,
	DICT_CMD_HELP,
	DICT_CMD_MATCH,
	DICT_CMD_OPTION,
	DICT_CMD_QUIT,
	DICT_CMD_SASLAUTH,
	DICT_CMD_SHOW_DATABASES,
	DICT_CMD_SHOW_INFO,
	DICT_CMD_SHOW_SERVER,
	DICT_CMD_SHOW_STRAT,
	DICT_CMD_STATUS,
	N_DICT_CMD
} dict_cmd_t;

typedef struct {
	dict_cmd_t cmd;
	gchar *arg;
	GSList *res;
} dict_req_t;

typedef struct {
	net_t *net;
	gchar *db;
	gchar *strat;
	GAsyncQueue *cancel_queue;
} dict_t;

void dict_cancel_clear(dict_t *);
gboolean dict_close(dict_t *, GError **);
gboolean dict_cmd(dict_t *, dict_req_t *, GError **);
void dict_cmd_cancel(dict_t *);
const gchar *dict_get_host(dict_t *);
gint dict_cmd_response_code(const gchar *);
void dict_destroy(dict_t *);
dict_t *dict_new(void);
gboolean dict_open(dict_t *, GError **);
void dict_req_destroy(dict_req_t *);
dict_req_t *dict_req_new(dict_cmd_t, gchar *);
void dict_set_db(dict_t *, const gchar *);
void dict_set_host(dict_t *, const gchar *);
void dict_set_strat(dict_t *, const gchar *);

#endif

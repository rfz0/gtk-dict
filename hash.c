#include "compat.h"
#include "hash.h"

void
hash_bucket_free(gpointer key,
		gpointer value)
{
	g_slist_free_full(value, g_free);
}

void
hash_bucket_reverse(gpointer key,
		gpointer value,
		gpointer user_data)
{
	g_hash_table_replace(user_data, g_strdup(key),
			g_slist_reverse(value));
}

Simple GTK DICT protocol client.

The DICT protocol can sometimes be more useful than using a search engine or
online encyclopedia because the protocol supports the notion of databases and
strategies. For example, it possible to search only the "jargon" database using
the "regexp" strategy, or to search all databases using the "soundex" strategy
for words that sound similar.

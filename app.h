#ifndef APP_H
#define APP_H

#include <gtk/gtk.h>

#include "dict.h"
#include "hist.h"

void app_init(void);
void app_create(void);
void app_destroy(void);

#endif

#include "compat.h"
#include "hist.h"

void
history_append(hist_t *h, gpointer data)
{
	if (h->index >= 0) {
		GSList *slist = g_slist_nth(h->data, h->index);
		if (slist && slist->next) {
			g_slist_free_full(slist->next, h->free_func);
			slist->next = NULL;
		}
	}
	h->data = g_slist_append(h->data, data);
	++h->index;
}

gboolean
history_at_head(hist_t *h)
{
	return (h->index == 0);
}

gboolean
history_at_tail(hist_t *h)
{
	return ((h->index + 1) == (gint)g_slist_length(h->data));
}

gboolean
history_backward(hist_t *h)
{
	if (h->index <= 0)
		return FALSE;
	--h->index;

	return TRUE;
}

void
history_clear(hist_t *h)
{
	g_slist_free_full(h->data, h->free_func);
	h->data = NULL;
	h->index = -1;
}

gpointer
history_data(hist_t *h)
{
	return g_slist_nth_data(h->data, h->index);
}

void
history_destroy(hist_t *h)
{
	if (h->data)
		g_slist_free_full(h->data, h->free_func);
	g_free(h);
}

gboolean
history_forward(hist_t *h)
{
	guint length = g_slist_length(h->data);
	if ((h->index + 1) == (gint)length)
		return FALSE;
	++h->index;

	return TRUE;
}

gboolean
history_is_empty(hist_t *h)
{
	return (h->data == NULL || h->index < 0);
}

hist_t *
history_new(GDestroyNotify free_func)
{
	hist_t *h = g_new0(hist_t, 1);
	h->data = NULL;
	h->index = -1;
	h->free_func = free_func;

	return h;
}
